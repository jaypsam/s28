db.hotel.insertOne({
	"name": "single",
	"acommodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic neccesities",
	"rooms_available": "10",
	"isAvailable": false
});

db.hotel.insertMany([
		{
	"name": "double",
	"acommodates": 3,
	"price": 2000,
	"description": "A room fit for small family on vacation",
	"rooms_available": "5",
	"isAvailable": false
		},
		{
	"name": "queen",
	"acommodates": 4,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for simple getaway",
	"rooms_available": "15",
	"isAvailable": false		}
	]);

db.hotel.find({
	"name": "double"
});

db.hotel.updateOne(
	{
	"rooms_available": "15"
	},
	{
		$set: {
	"rooms_available": "0"
		}
	})

db.hotel.deleteMany({
    "rooms_available": "0"
})